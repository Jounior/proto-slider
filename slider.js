class Slider{

    constructor(){
        this.slider_selector = document.getElementById('slider');
        this.numberShowImages = 5;
        this.lastImage = 5
        this.images = [
            'images/1.jpg',
            'images/2.jpg',
            'images/3.jpg',
            'images/4.jpg',
            'images/5.jpg',
            'images/6.jpg',
            'images/7.jpg',
            'images/8.jpg',
            'images/9.jpg',
            'images/10.jpg',
        ]
    }

    createImage(url){
        let img = document.createElement('img');
        img.src = url;

        return img;
    }

    appendImage(img){
        this.slider_selector.appendChild(img);
    }

    removeImage(){
        let first_image =  this.slider_selector.firstElementChild;
        this.slider_selector.removeChild(first_image);
    }
    
    drawImages(){
        for( let i = 0 ; i <= this.numberShowImages ; i++ ){
            this.appendImage(this.createImage(this.images[i]));
        }
    }
    
    sliceToRight(){
        if (this.lastImage == this.images.length - 1 ){
            this.lastImage = 0;
        }

        let nextSelection = this.images[this.lastImage + 1]

        let image = this.createImage(nextSelection)
        this.appendImage(image)
        this.removeImage()

        this.lastImage += 1;       

    }
}

const slider = new Slider();
slider.drawImages();